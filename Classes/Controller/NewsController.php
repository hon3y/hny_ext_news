<?php
namespace HIVE\HiveExtNews\Controller;

/***
 *
 * This file is part of the "hive_ext_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * NewsController
 */
class NewsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * newsRepository
     *
     * @var \HIVE\HiveExtNews\Domain\Repository\NewsRepository
     * @inject
     */
    protected $newsRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $news = $this->newsRepository->findAll();
        $this->view->assign('news', $news);
    }

    /**
     * action show
     *
     * @param \HIVE\HiveExtNews\Domain\Model\News $news
     * @return void
     */
    public function showAction(\HIVE\HiveExtNews\Domain\Model\News $news)
    {
        $this->view->assign('news', $news);
    }
}
