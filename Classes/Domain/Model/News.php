<?php
namespace HIVE\HiveExtNews\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_news" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * News
 */
class News extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * header
     *
     * @var string
     */
    protected $header = '';

    /**
     * featuredNews
     *
     * @var bool
     */
    protected $featuredNews = false;

    /**
     * teaser
     *
     * @var string
     */
    protected $teaser = '';

    /**
     * authorName
     *
     * @var string
     */
    protected $authorName = '';

    /**
     * authorEmail
     *
     * @var string
     */
    protected $authorEmail = '';

    /**
     * authorAdress
     *
     * @var string
     */
    protected $authorAdress = '';

    /**
     * authorImg
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $authorImg = null;

    /**
     * date
     *
     * @var \DateTime
     */
    protected $date = null;

    /**
     * content
     *
     * @var string
     */
    protected $content = '';

    /**
     * thumbnail
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $thumbnail = null;

    /**
     * contentImg
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $contentImg = null;

    /**
     * contentImgCaption
     *
     * @var string
     */
    protected $contentImgCaption = '';

    /**
     * Returns the header
     *
     * @return string $header
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Sets the header
     *
     * @param string $header
     * @return void
     */
    public function setHeader($header)
    {
        $this->header = $header;
    }

    /**
     * Returns the featuredNews
     *
     * @return bool $featuredNews
     */
    public function getFeaturedNews()
    {
        return $this->featuredNews;
    }

    /**
     * Sets the featuredNews
     *
     * @param bool $featuredNews
     * @return void
     */
    public function setFeaturedNews($featuredNews)
    {
        $this->featuredNews = $featuredNews;
    }

    /**
     * Returns the boolean state of featuredNews
     *
     * @return bool
     */
    public function isFeaturedNews()
    {
        return $this->featuredNews;
    }

    /**
     * Returns the teaser
     *
     * @return string $teaser
     */
    public function getTeaser()
    {
        return $this->teaser;
    }

    /**
     * Sets the teaser
     *
     * @param string $teaser
     * @return void
     */
    public function setTeaser($teaser)
    {
        $this->teaser = $teaser;
    }

    /**
     * Returns the authorName
     *
     * @return string $authorName
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * Sets the authorName
     *
     * @param string $authorName
     * @return void
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
    }

    /**
     * Returns the authorEmail
     *
     * @return string $authorEmail
     */
    public function getAuthorEmail()
    {
        return $this->authorEmail;
    }

    /**
     * Sets the authorEmail
     *
     * @param string $authorEmail
     * @return void
     */
    public function setAuthorEmail($authorEmail)
    {
        $this->authorEmail = $authorEmail;
    }

    /**
     * Returns the authorAdress
     *
     * @return string $authorAdress
     */
    public function getAuthorAdress()
    {
        return $this->authorAdress;
    }

    /**
     * Sets the authorAdress
     *
     * @param string $authorAdress
     * @return void
     */
    public function setAuthorAdress($authorAdress)
    {
        $this->authorAdress = $authorAdress;
    }

    /**
     * Returns the authorImg
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $authorImg
     */
    public function getAuthorImg()
    {
        return $this->authorImg;
    }

    /**
     * Sets the authorImg
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $authorImg
     * @return void
     */
    public function setAuthorImg(\TYPO3\CMS\Extbase\Domain\Model\FileReference $authorImg)
    {
        $this->authorImg = $authorImg;
    }

    /**
     * Returns the date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the date
     *
     * @param \DateTime $date
     * @return void
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Returns the content
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets the content
     *
     * @param string $content
     * @return void
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Returns the thumbnail
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $thumbnail
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Sets the thumbnail
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $thumbnail
     * @return void
     */
    public function setThumbnail(\TYPO3\CMS\Extbase\Domain\Model\FileReference $thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * Returns the contentImg
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $contentImg
     */
    public function getContentImg()
    {
        return $this->contentImg;
    }

    /**
     * Sets the contentImg
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $contentImg
     * @return void
     */
    public function setContentImg(\TYPO3\CMS\Extbase\Domain\Model\FileReference $contentImg)
    {
        $this->contentImg = $contentImg;
    }

    /**
     * Returns the contentImgCaption
     *
     * @return string $contentImgCaption
     */
    public function getContentImgCaption()
    {
        return $this->contentImgCaption;
    }

    /**
     * Sets the contentImgCaption
     *
     * @param string $contentImgCaption
     * @return void
     */
    public function setContentImgCaption($contentImgCaption)
    {
        $this->contentImgCaption = $contentImgCaption;
    }
}
